import { createApp } from 'vue'
import App from './App.vue'
import { myLibDbc } from './init.js'
import 'element-plus/dist/index.css'
import 'element-plus/theme-chalk/dark/css-vars.css'
import {
    create,
    NEllipsis
} from 'naive-ui'

const naive = create({ components: [NEllipsis] }) 

myLibDbc.mount().then(() =>createApp(App).use(naive).mount('#app')).catch()