# my-lib

#### 介绍
个人资源库,标签定义,索引定义,搜索,资源预览

#### 软件架构
- 本软件主体使用vue,element plus,native ui开发,主体运行在渲染进程
- indexDB作为数据库
- electronic
包含页面(dist)的主体源码
- 页面源码
    1. my-lib-favorite
    2. my-lib-filter_index_table
    3. my-lib-index_edit
    4. my-lib-main
    5. my-lib-review
    6. my-lib-tag_management
#### 安装教程

1.  electron文件夹下执行npm run make
2.  生成安装包和软件主体