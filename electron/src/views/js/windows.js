class MyLibWindowCommunication {
    openWindow(url, title) {
        return new Promise((resolve, reject) => {
            try {
                // 打开一个新页面
                window.open(url, title);
                // 监听
                window.addEventListener('message', event => {
                    resolve(event)
                })
            } catch (error) {
                reject(error)
            }
        })
    }
    openWindowAndSession(url, title, handle = (event) => true) {
        return new Promise((resolve, reject) => {
            try {
                // 打开一个新页面
                const windowInstance = window.open(url, title);
                // 监听
                window.addEventListener('message', event => {
                    if (handle(windowInstance, event))
                        resolve()
                })
            } catch (error) {
                reject(error)
            }
        })
    }
    sendToParent(args) {
        window.opener.postMessage(args);
    }
    sendToParentAndClose(args) {
        window.opener.postMessage(args);
        window.close();
    }
    close() {
        window.opener.postMessage({ handle: 0 });
        window.close();
    }
}

function getUrlQuery() {
    const url = window.location.href
    // \w+ 表示匹配至少一个(数字、字母及下划线), [\u4e00-\u9fa5]+ 表示匹配至少一个中文字符
    const pattern = /(\w+|[\u4e00-\u9fa5]+)=(\w+|[\u4e00-\u9fa5]+)/ig;
    const result = {};
    url.replace(pattern, ($, $1, $2) => {
        result[$1] = $2;
    })
    return result
}

const $UrlQuery = getUrlQuery()


