class IndexFileRelationship {
    indexId
    fileId
    sequence
    constructor(indexId, fileId, sequence) {
        this.indexId = indexId
        this.fileId = fileId
        this.sequence = sequence
    }
}
class IndexCoverRelationship {
    indexId
    fileId
    sequence
    constructor(indexId, fileId, sequence) {
        this.indexId = indexId
        this.fileId = fileId
        this.sequence = sequence
    }
}
class IndexEntity {
    title
    // 副标题
    subtitle
    // 描述
    description
    // 内容创作时间
    releaseDate
    // 索引创建时间
    createDate
    // 点击次数
    clickTimes
    // 状态 (0已删除 1正常 2临时 3待审核)
    state
    // 索引类型(0未知 1视频 2视频集 3图片 4文本 5文本集 6音频)(索引-文件、索引)单一(索引-文件)
    type
    // 评分
    rate
    constructor(title, subtitle, description, releaseDate, createDate, clickTimes, state, type, rate) {
        this.title = title
        this.subtitle = subtitle
        this.description = description
        this.releaseDate = releaseDate
        this.createDate = createDate
        this.clickTimes = clickTimes
        this.state = state
        this.type = type
        this.rate = rate
    }
}
class IndexTagRelationship {
    indexId
    tagId
    constructor(indexId, tagId) {
        this.indexId = indexId
        this.tagId = tagId
    }
}
class IndexSubIndexRelationship {
    indexId
    subIndexId
    constructor(indexId, subIndexId) {
        this.indexId = indexId
        this.subIndexId = subIndexId
    }
}
class TagEntity {
    text
    state
    createDate
    type
    constructor(text, state, type, createDate) {
        this.text = text
        this.state = state
        this.type = type
        this.createDate = createDate
    }
}
class FavoriteEntity {
    createDate
    indexId
    constructor(indexId, createDate) {
        this.indexId = indexId
        this.createDate = createDate
    }
}
class OperateRecordEntity {
    timeOn
    timeUP
    state
    operate
    table
    params
    result
    /**
     * 
     * @param {Date} timeOn 开始时间 
     * @param {Date} timeUP 结束时间
     * @param {Number} state 执行状态
     * @param {Number} operate 操作类型
     * @param {String} table 表
     * @param {*} params 参数
     * @param {*} result 结果
     */
    constructor(timeOn, timeUP, state, operate, table, params, result) {
        this.timeOn = timeOn
        this.timeUP = timeUP
        this.state = state
        this.operate = operate
        this.table = table
        this.params = params
        this.result = result
    }
}
//TODO 检查避免非字符数字值作为键
class TableComplexIndex {
    name
    columnSequence
    isUq = false
    constructor(name, columnSequence, isUq) {
        this.name = name
        this.columnSequence = columnSequence
        this.isUq = isUq
    }
}
class TableSimpleIndex {
    column
    isUq = false
    constructor(column, isUq) {
        this.name = name
        this.column = column
        this.isUq = isUq
    }
}
class Table {
    name
    columnSequence
    complexIndexSet
    simpleIndexSet
    constructor(name, columnSequence, complexIndexSet, simpleIndexSet) {
        this.name = name
        this.columnSequence = columnSequence
        this.complexIndexSet = complexIndexSet
        this.simpleIndexSet = simpleIndexSet
    }
}
class SearchHistoryEntity {
    key
    type
    mode
    searchTime
    constructor(key, type, mode, searchTime) {
        this.key = key
        this.type = type
        this.mode = mode
        this.searchTime = searchTime
    }
}
const $MyLibSearch_history = new Table(
    'search_history',
    new Set(['id', 'key', 'type', 'mode', 'searchTime']),
    new Set(),
    new Set()
)

const $MyLibOperate_record = new Table(
    'operate_record',
    new Set(['id', 'timeOn', 'timeUP', 'state', 'operate', 'table', 'params', 'result']),
    new Set(),
    new Set([
        new TableSimpleIndex('id', true),
        new TableSimpleIndex('timeOn', false),
        new TableSimpleIndex('timeUP', false),
        new TableSimpleIndex('state', false),
        new TableSimpleIndex('operate', false),
        new TableSimpleIndex('table', false),
        new TableSimpleIndex('params', false),
        new TableSimpleIndex('result', false)
    ])
)

const $MyLibFavorite = new Table(
    'favorite',
    new Set(['id', 'indexId', 'createDate']),
    new Set(),
    new Set([
        new TableSimpleIndex('id', true),
        new TableSimpleIndex('indexId', true),
        new TableSimpleIndex('createDate', false)
    ])
)

const $MyLibIndex_tag_relationship = new Table(
    'index_tag_relationship',
    new Set(['id', 'indexId', 'tagId']),
    new Set(),
    new Set([
        new TableSimpleIndex('id', true),
        new TableSimpleIndex('indexId', false),
        new TableSimpleIndex('tagId', false),
    ])
)

const $MyLibIndex_file_relationship = new Table(
    'index_file_relationship',
    new Set(['id', 'indexId', 'fileId', 'sequence']),
    new Set([
        new TableComplexIndex('indexIdFileId', ['indexId', 'fileId'], true)
    ]),
    new Set([
        new TableSimpleIndex('id', true),
        new TableSimpleIndex('indexId', false),
        new TableSimpleIndex('fileId', false),
    ])
)

const $MyLibIndex_cover_relationship = new Table(
    'index_cover_relationship',
    new Set(['id', 'indexId', 'fileId', 'sequence']),
    new Set([
        new TableComplexIndex('indexIdSequence', ['indexId', 'fileId'], true)
    ]),
    new Set([
        new TableSimpleIndex('id', true),
        new TableSimpleIndex('indexId', false),
        new TableSimpleIndex('fileId', false)
    ])
)
const $MyLibIndex_subindex_relationship = new Table(
    'index_subindex_relationship',
    new Set(['id', 'indexId', 'subIndexId']),
    new Set(),
    new Set([
        new TableSimpleIndex('id', true),
        new TableSimpleIndex('indexId', false),
        new TableSimpleIndex('subIndexId', false)
    ])
)
const $MyLibTag = new Table(
    'tag',
    new Set(['id', 'text', 'type', 'createDate', 'state']),
    new Set([
        new TableComplexIndex('textState', ['text', 'state'], true)
    ]),
    new Set([
        new TableSimpleIndex('id', true),
        new TableSimpleIndex('text', true),
        new TableSimpleIndex('type', false),
        new TableSimpleIndex('createDate', false),
        new TableSimpleIndex('state', false)
    ])
)

const $MyLibIndex = new Table(
    'index',
    new Set(['id', 'title', 'subtitle', 'description', 'releaseDate', 'createDate', 'clickTimes', 'state', 'type', 'rate']),
    new Set([
        new TableComplexIndex('idStateType', ['id', 'state', 'type'], true),
        new TableComplexIndex('titleStateType', ['title', 'state', 'type'], false),
        new TableComplexIndex('subTitleStateType', ['subTitle', 'state', 'type'], false),
        new TableComplexIndex('stateType', ['state', 'type'], false),
        //file
        new TableComplexIndex('idType', ['id', 'type'], true),
        new TableComplexIndex('titleType', ['title', 'type'], false),
        new TableComplexIndex('subTitleType', ['subTitle', 'type'], false),
    ]),
    new Set([
        new TableSimpleIndex('id', true),
        new TableSimpleIndex('title', true),
        new TableSimpleIndex('subtitle', true),
        new TableSimpleIndex('releaseDate', false),
        new TableSimpleIndex('createDate', false),
        new TableSimpleIndex('clickTimes', false),
        new TableSimpleIndex('state', false),
        new TableSimpleIndex('type', false),
        new TableSimpleIndex('rate', false)
    ])
)

const $MyLibFile = new Table(
    'file',
    new Set([
        'id',
        'dev', 'mode', 'nlink', 'uid', 'gid', 'rdev', 'blksize', 'ino', 'size', 'blocks', 'birthtimeMs',
        'root', 'name', 'dir', 'ext'
    ]),
    new Set([
        new TableComplexIndex('dirNameExt', ['dir', 'name', 'ext'], true)
    ]),
    new Set([
        new TableSimpleIndex('id', true),
    ])
)
const $MyLibTables = new Set([
    $MyLibFile,
    $MyLibIndex,
    $MyLibTag,
    $MyLibIndex_subindex_relationship,
    $MyLibIndex_cover_relationship,
    $MyLibIndex_file_relationship,
    $MyLibIndex_tag_relationship,
    $MyLibFavorite,
    $MyLibOperate_record,
    $MyLibSearch_history
])
const $MyLibFileFieldName = new Map([
    ['ext', '文件扩展名'],
    ['root', '根目录'],
    ['name', '文件名'],
    ['dir', '所在文件夹'],
    ['dev', '包含该文件设备数字标识符'],
    ['mode', '描述文件类型和模式位字段'],
    ['nlink', '文件存在硬链接数'],
    ['uid', '拥有该文件用户数字用户标识符（POSIX）'],
    ['gid', '拥有该文件组数字组标识符（POSIX）'],
    ['rdev', '若文件被视为特殊文件，该值为数字设备标识符'],
    ['blksize', 'I/O操作文件系统块大小'],
    ['ino', '文件系统特定“Inode”编号'],
    ['size', '文件大小（字节）'],
    ['blocks', '文件分配的块数'],
    ['atimeMs', '上次访问文件时间戳'],
    ['mtimeMs', '上次修改文件时间戳'],
    ['ctimeMs', '上次更改文件状态时间戳'],
    ['birthtimeMs', '创建时间戳']
])
const $MyLibIndexFieldName = new Map([
    ['id', 'id'],
    ['isid', '关联ID'],
    ['title', '主标题'],
    ['subtitle', '副标题'],
    ['description', '描述'],
    ['releaseDate', '内容创作时间'],
    ['createDate', '索引创建时间'],
    ['clickTimes', '点击次数'],
    ['state', '状态'],
    ['type', '索引类型'],
    ['nodeType', '节点类型'],
    ['rate', '评分']
])
const $MyLibTagFieldName = new Map([
    ['id', 'id'],
    ['text', '文本'],
    ['state', '状态'],
    ['type', '类型'],
    ['createDate', '创建时间']
])