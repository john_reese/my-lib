
class Params {
    constructor() {
        // String
        this.properties = new Set()
        this.filters = new Set()
    }
    setTitle(title) {
        this.title = new String(title)
    }
    // 允许选择文件
    setIsOpenFile() {
        this.properties.delete("openDirectory")
        this.properties.add("openFile")
    }
    // 允许多选
    setIsMultiSelections() {
        this.properties.add("multiSelections")
    }
    // 允许选择文件夹
    setIsOpenDirectory() {
        this.properties.delete("openFile")
        this.properties.add("openDirectory")
    }
    // 显示对话框中的隐藏文件
    setShowHiddenFiles() {
        this.properties.add("showHiddenFiles")
    }
    setFilter(extension) {
        this.filters.add(extension)
        // filters: [
        //   { name: 'Images', extensions: ['jpg', 'png', 'gif'] },
        //   { name: 'Movies', extensions: ['mkv', 'avi', 'mp4'] },
        //   { name: 'Custom File Type', extensions: ['as'] },
        //   { name: 'All Files', extensions: ['*'] }
        // ]
    }
    setImgFilter() {
        this.properties.delete("openDirectory")
        this.filters.add({ name: '', extensions: ['jpg', 'png', 'gif', 'bmp', 'jpeg', 'pdf'] })
    }
    finsh() {
        this.properties = [...this.properties, 'dontAddToRecent']
        this.filters = [...this.filters]
        this.title = this.title.toString()
    }
}
const $Render = {
    getFile: (params) => window.electronAPI.getFile(params),
    getImage: (params) => window.electronAPI.getImage(params),
    getStoreCopy: () => {
        const param = new Params()
        param.setIsOpenFile()
        param.setFilter({ name: 'Copy', extensions: ['json'] })
        param.setShowHiddenFiles()
        param.setTitle('选择备份')
        param.finsh()
        return window.electronAPI.getStoreCopy(param)
    },
    getConfig: () => window.electronAPI.getConfig(),
    putConfig: params => window.electronAPI.putConfig(params),
    putAccount: params => window.electronAPI.putAccount(params),
    exportStore: param => window.electronAPI.exportStore(param),
    exportStoreAs: param => {
        const dialogArgs = new Params()
        dialogArgs.setTitle('数据库备份到')
        dialogArgs.finsh()
        return window.electronAPI.exportStoreAs([param, dialogArgs])
    },
}