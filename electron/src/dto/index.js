class DTRO {
    state
    value
    constructor(state = 0, value) {
        this.state = state //0:异常 1:正常,有返回值 2:正常,无返回值
        this.value = value
    }
}

class ParentAndChild {
    constructor(parentEntity,entity) {
        this.entity = entity
        this.parentEntity = parentEntity
    }
}
exports.ParentAndChild = ParentAndChild
exports.DTRO = DTRO