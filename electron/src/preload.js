const { contextBridge, ipcRenderer } = require('electron')


contextBridge.exposeInMainWorld('electronAPI', {
  getFile: params => ipcRenderer.invoke('dialog:getFile', params),
  getImage: params => ipcRenderer.invoke('dialog:getImage', params),
  getLibList: () => ipcRenderer.invoke('system:getLibList'),
  getConfig: () => ipcRenderer.invoke('config:get'),
  putConfig: params => ipcRenderer.invoke('config:put', params),

  putAccount: params => ipcRenderer.invoke('config:putAccount', params),
  exportStore: param => ipcRenderer.invoke('store:export', param),
  exportStoreAs: params => ipcRenderer.invoke('store:exportAs', params),
  getStoreCopy: params => ipcRenderer.invoke('store:getCopy', params),
  windowBlur: callback => ipcRenderer.on('window:blur', callback),
  windowFocus: callback => ipcRenderer.on('window:focus', callback)
  // 能暴露的不仅仅是函数，我们还可以暴露变量
})
