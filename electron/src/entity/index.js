const Path = require('path')
const statKeys = new Array('dev', 'mode', 'nlink', 'uid', 'gid', 'rdev', 'blksize', 'ino', 'size', 'blocks', 'birthtimeMs')
const pathKeys = new Array('root', 'name', 'dir', 'ext')

class FileEntity {
    constructor(path, stats) {
        const pathObj = Path.parse(path)
        pathKeys.forEach(key => {
            this[key] = pathObj[key]
        })
        statKeys.forEach(key => {
            this[key] = stats[key]
        })
    }
}

class Config {
    username
    password
    backup_path
    session_path
    constructor(username, password, backup_path, session_path){
        this.username = username
        this.password = password
        this.backup_path = backup_path
        this.session_path = session_path
    }
}
exports.Config = Config
exports.FileEntity = FileEntity