const { Worker } = require('worker_threads');

const getThreadSize = taskSize =>
    taskSize <= 24 ? 1 :
        taskSize <= 32 ? 2 :
            taskSize <= 48 ? 3 :
                taskSize <= 64 ? 4 :
                    taskSize <= 80 ? 5 :
                        taskSize <= 96 ? 6 :
                            taskSize <= 112 ? 7 :
                                taskSize <= 128 ? 8 :
                                    taskSize <= 144 ? 9 :
                                        taskSize <= 160 ? 10 : 12

class Thread {
    constructor(taskPath, workerData) {
        this.taskPath = taskPath
        this.workerData = workerData
    }
    run() {
        return new Promise((resolve, reject) => {
            const worker = new Worker(this.taskPath, { workerData: this.workerData });
            worker.once("message", resolve);
            worker.on("error", reject);
            worker.on("exit", code => {
                if (code !== 0) {
                    console.log('exit:' + code)
                }
            })
        })
    }
}

exports.getThreadSize = getThreadSize
exports.Thread = Thread