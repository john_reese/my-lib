const { parentPort, workerData, isMainThread } = require('worker_threads')
const fs = require('fs')
const { FileEntity } = require('../../entity/index.js')

function mission(pathList) {
    try {
        return pathList.map(path => JSON.stringify(new FileEntity(path, fs.statSync(path))))
    } catch (e) {
        console.log(e)
    }
}

parentPort.postMessage(mission(workerData))