import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'

import Login from '../views/login/LoginView.vue'

import Setting from '../views/setting/SettingView.vue'
import MetaInfo from '../views/meta_info/MetaInfoView.vue'
import Index from '../views/IndexView.vue'
import SearchTextResult from '../views/search_text_result/SearchTextResultView.vue'
import SearchPictureResult from '../views/search_picture_result/SearchPictureResultView.vue'
import Overview from '../views/overview/OverviewView.vue'
import Empty from '../views/empty/EmptyView.vue'
import PictureListDetail from '../views/picture_list_detail/PictureListDetailView.vue'
import PicturePreviewDetail from '../views/picture_preview_detail/PicturePreviewDetailView.vue'
import TextDetail from '../views/text_detail/TextDetailView.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    component: Index,
    redirect: { path: '/overview' },
    children: [
      {
        path: 'overview',
        component: Overview,
        meta: {
          require_authorize: true
        }
      },
      {
        path: 'search_text_result',
        component: SearchTextResult,
        meta: {
          require_authorize: true
        }
      },
      {
        path: 'search_picture_result',
        component: SearchPictureResult,
        meta: {
          require_authorize: true
        }
      },
      {
        path: 'meta_info',
        component: MetaInfo,
        meta: {
          require_authorize: true
        }
      },
      {
        path: 'setting',
        component: Setting,
        meta: {
          require_authorize: true
        }
      },
      {
        path: 'content_empty',
        component: Empty,
        meta: {
          require_authorize: true
        }
      },
      {
        path: 'result_empty',
        component: Empty,
        meta: {
          require_authorize: true
        }
      },
      {
        path: 'picture_list_detail',
        component: PictureListDetail,
        meta: {
          require_authorize: true
        }
      },
      {
        path: 'picture_preview_detail',
        component: PicturePreviewDetail,
        meta: {
          require_authorize: true
        }
      },
      {
        path: 'text_detail',
        component: TextDetail,
        meta: {
          require_authorize: true
        }
      }
    ]
  },
  {
    path: '/login',
    component: Login
  },
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  if (to.meta.require_authorize) {
    const is_authorized = window.sessionStorage.getItem('is_authorized')
    if (is_authorized === 'authorized')
      next()
    else
      next('/login')
  } else
    next()
})

export default router