import { createApp, App as app } from 'vue'
import App from './App.vue'
import router from './router'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import 'element-plus/dist/index.css'
import { Search } from "@/search/index.js"
import { myLibDbc, render } from './init.js'

import {
  // create naive ui
  create,
  // component
  NCollapse,
  NCollapseItem,
  NDropdown,
  NEllipsis,
  NModal,
  NCard,
} from 'naive-ui'
const naive = create({
  components: [NModal, NCard, NEllipsis, NCollapse, NCollapseItem, NDropdown]
})

import 'element-plus/theme-chalk/dark/css-vars.css'
import errorStore from '@/store/index.js'

const $APP = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  $APP.component(key, component)
}

myLibDbc.mount()
  .then(
    async () => {
      const config = await render.getConfig()
      $APP
        .provide('accountPassword', {
          username: config.username,
          password: config.password
        })
        .use(naive)
        .use(
          {
            install(app: app, search: Search) {
              app.config.globalProperties.$search = search
            }
          },
          new Search(myLibDbc)
        )
        .use(router)
        .use(errorStore)
        .mount('#app')
    })
  .catch((error: Error) => {
    alert(error)
    console.log(error)
  })