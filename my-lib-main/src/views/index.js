import { ElMessageBox } from 'element-plus'

const emptyFunction = () => {/**/ }
const empty = (...param) => {/**/ };
class Filter {
    static defaultFilter = new Filter()
    conditions = new Array(6)
    current = 0
    constructor(correlation = 0, releaseDate = 0, createDate = 0, clickTimes = 0, score = 0, state = 4) {
        this.conditions[0] = correlation
        this.conditions[1] = releaseDate
        this.conditions[2] = createDate
        this.conditions[3] = clickTimes
        this.conditions[4] = score
        this.conditions[5] = state
    }
    getTypeVal(index) {
        return this.conditions[index]
    }
    reorder(type, val, list) {
        let sortList
        switch (type) {
            case 0:
                this.current = 0
                this.conditions[0] = val
                this.conditions[1] = this.conditions[2] = this.conditions[3] = this.conditions[4] = 0
                sortList = val === 1 ? list.sort((a, b) => b.source.totalScore - a.source.totalScore) : list.sort((a, b) => a.source.totalScore - b.source.totalScore)
                return this.conditions[5] === 4 ? sortList : sortList.filter(index => index.source.state === this.conditions[5])
            case 1:
                this.current = 1
                this.conditions[1] = val
                this.conditions[0] = this.conditions[2] = this.conditions[3] = this.conditions[4] = 0
                sortList = val === 1 ? list.sort((a, b) => b.source.releaseDate - a.source.releaseDate) : list.sort((a, b) => a.source.releaseDate - b.source.releaseDate)
                return this.conditions[5] === 4 ? sortList : sortList.filter(index => index.source.state === this.conditions[5])
            case 2:
                this.current = 2
                this.conditions[2] = val
                this.conditions[0] = this.conditions[1] = this.conditions[3] = this.conditions[4] = 0
                sortList = val === 1 ? list.sort((a, b) => b.source.createDate - a.source.createDate) : list.sort((a, b) => a.source.createDate - b.source.createDate)
                return this.conditions[5] === 4 ? sortList : sortList.filter(index => index.source.state === this.conditions[5])
            case 3:
                this.current = 3
                this.conditions[3] = val
                this.conditions[0] = this.conditions[1] = this.conditions[2] = this.conditions[4] = 0
                sortList = val === 1 ? list.sort((a, b) => b.source.clickTimes - a.source.clickTimes) : list.sort((a, b) => a.source.clickTimes - b.source.clickTimes)
                return this.conditions[5] === 4 ? sortList : sortList.filter(index => index.source.state === this.conditions[5])
            case 4:
                this.current = 4
                this.conditions[4] = val
                this.conditions[0] = this.conditions[1] = this.conditions[2] = this.conditions[3] = 0
                sortList = val === 1 ? list.sort((a, b) => b.source.rate - a.source.rate) : list.sort((a, b) => a.source.rate - b.source.rate)
                return this.conditions[5] === 4 ? sortList : sortList.filter(index => index.source.state === this.conditions[5])
            case 5:
                this.conditions[5] = val
                switch (this.current) {
                    case 0:
                        sortList = val === 1 ? list.sort((a, b) => b.source.totalScore - a.source.totalScore) : list.sort((a, b) => a.source.totalScore - b.source.totalScore)
                        return this.conditions[5] === 4 ? sortList : sortList.filter(index => index.source.state === this.conditions[5])
                    case 1:
                        sortList = val === 1 ? list.sort((a, b) => b.source.releaseDate - a.source.releaseDate) : list.sort((a, b) => a.source.releaseDate - b.source.releaseDate)
                        return this.conditions[5] === 4 ? sortList : sortList.filter(index => index.source.state === this.conditions[5])
                    case 2:
                        sortList = val === 1 ? list.sort((a, b) => b.source.createDate - a.source.createDate) : list.sort((a, b) => a.source.createDate - b.source.createDate)
                        return this.conditions[5] === 4 ? sortList : sortList.filter(index => index.source.state === this.conditions[5])
                    case 3:
                        sortList = val === 1 ? list.sort((a, b) => b.source.clickTimes - a.source.clickTimes) : list.sort((a, b) => a.source.clickTimes - b.source.clickTimes)
                        return this.conditions[5] === 4 ? sortList : sortList.filter(index => index.source.state === this.conditions[5])
                    case 4:
                        sortList = val === 1 ? list.sort((a, b) => b.source.rate - a.source.rate) : list.sort((a, b) => a.source.rate - b.source.rate)
                        return this.conditions[5] === 4 ? sortList : sortList.filter(index => index.source.state === this.conditions[5])
                }
        }
    }
}
class NoneView {
    static defaultView = new NoneView
}
class ListView {
    static defaultView = new ListView()
    filter
    path
    total
    viewMode
    data
    page
    type
    mode
    searchStr
    handleTabSwitch
    handleTabClose
    constructor(data = [], page = [], viewMode = false, handleTabSwitch = empty, handleTabClose = empty, type = 0, mode = 0, searchStr = new String()) {
        this.total = data.length
        this.data = data
        this.page = page
        this.viewMode = viewMode
        this.type = type
        this.mode = mode
        this.path = viewMode ? '/search_picture_result' : '/search_text_result'
        this.filter = new Filter()
        this.searchStr = searchStr
        this.handleTabSwitch = handleTabSwitch
        this.handleTabClose = handleTabClose
    }
    patchViewMode(val) {
        this.viewMode = val
        this.path = val ? '/search_picture_result' : '/search_text_result'
    }
}
class MetaView {
    static url = '/meta_info'
    static defaultView = new MetaView()
    path = MetaView.url
    index
    // [关系主键,被关联entity]
    subindexList
    fileList
    tagList
    authorList
    coverList
    favorite
    handleTabSwitch
    handleTabClose
    constructor(index = new IndexEntity('', '', '', '', '', '', '', '', '', 0), subindexList = [], fileList = [], tagList = [], authorList = [], coverList = [], favorite, handleTabSwitch = empty, handleTabClose = empty) {
        this.index = index
        this.subindexList = subindexList
        this.fileList = fileList
        this.tagList = tagList
        this.authorList = authorList
        this.coverList = coverList
        this.favorite = favorite
        this.handleTabSwitch = handleTabSwitch
        this.handleTabClose = handleTabClose
    }
}
class TextContentView {
    static url = '/text_detail'
    static defaultView = new TextContentView()
    path = TextContentView.url
    currentSection
    drawer
    textarea
    textEdit
    nextDisable
    upDisable
    index
    subindexList
    fileList
    tagList
    authorList
    coverList
    favorite
    handleTabSwitch
    handleTabClose
    constructor(currentSection = '0', drawer = false, textEdit = false, index = new IndexEntity('', '', '', '', '', '', '', '', '', 0), subindexList = [], fileList = [], tagList = [], authorList = [], coverList = [], favorite, handleTabSwitch = empty, handleTabClose = empty) {
        this.currentSection = currentSection
        this.drawer = drawer
        this.textEdit = textEdit
        this.textarea = subindexList.length > 0 ? subindexList[Number(currentSection)].description.repeat(1) : index.description.repeat(1)
        this.upDisable = true
        this.nextDisable = subindexList.length < 2
        this.index = index
        this.subindexList = subindexList
        this.fileList = fileList
        this.tagList = tagList
        this.authorList = authorList
        this.coverList = coverList
        this.favorite = favorite
        this.handleTabSwitch = handleTabSwitch
        this.handleTabClose = handleTabClose
    }
    nextSection() {
        const i = Number(this.currentSection) + 1
        this.currentSection = `${i}`
        this.nextDisable = i === this.subindexList.length - 1
        this.upDisable = !this.nextDisable
        this.textarea = this.subindexList[i].description.repeat(1)
    }
    upSection() {
        const i = Number(this.currentSection) - 1
        this.currentSection = `${i}`
        this.upDisable = i === 0
        this.nextDisable = !this.upDisable
        this.textarea = this.subindexList[i].description.repeat(1)
    }
    selectSection(index) {
        this.currentSection = index
        const i = Number(index)
        this.nextDisable = i === this.subindexList.length - 1
        this.upDisable = i === 0
        this.textarea = this.subindexList[i].description.repeat(1)
    }
    resetTextarea() {
        this.textarea = this.subindexList.length > 0 ? this.subindexList[Number(this.currentSection)].description.repeat(1) : this.index.description.repeat(1)
    }
    inputTextarea(val) {
        this.textarea = val
    }
    switchEdit(val) {
        this.textEdit = val
    }
    switchSectionDrawer(val) {
        this.drawer = val
    }

    static newInstanceByMetaView(metaView, handleTabSwitch = empty, handleTabClose = empty) {
        return new TextContentView('0', false, false, metaView.index, metaView.subindexList, metaView.fileList, metaView.tagList, metaView.authorList, metaView.coverList, metaView.favorite, handleTabSwitch, handleTabClose)
    }
}
class PictureContentView {
    static defaultView = new PictureContentView()
    path
    viewMode
    currentIndex
    index
    subindexList
    fileList
    tagList
    authorList
    coverList
    favorite
    handleTabSwitch
    handleTabClose
    constructor(index = new IndexEntity('', '', '', '', '', '', '', '', '', 0), subindexList = [], fileList = [], tagList = [], authorList = [], coverList = [], viewMode = false, currentIndex = 0, favorite, handleTabSwitch = empty, handleTabClose = empty) {
        this.index = index
        this.subindexList = subindexList
        this.fileList = fileList
        this.tagList = tagList
        this.authorList = authorList
        this.coverList = coverList
        this.viewMode = viewMode
        this.currentIndex = currentIndex
        this.path = viewMode ? '/picture_preview_detail' : '/picture_list_detail'
        this.favorite = favorite
        this.handleTabSwitch = handleTabSwitch
        this.handleTabClose = handleTabClose
    }
    patchViewMode(val) {
        this.viewMode = val
        this.path = val ? '/picture_preview_detail' : '/picture_list_detail'
    }
    updateCurrentIndex(index) {
        this.currentIndex = index
    }

    static newInstanceByMetaView(metaView, viewMode, handleTabSwitch = empty, handleTabClose = empty) {
        return new PictureContentView(metaView.index, metaView.subindexList, metaView.fileList, metaView.tagList, metaView.authorList, metaView.coverList, viewMode, 0, handleTabSwitch, handleTabClose)
    }
}
class ResultEmptyView {
    static url = '/result_empty'
    static defaultView = new ResultEmptyView()
    path = ResultEmptyView.url
    page = []
    data = []
    total = 0
    type
    mode
    position
    handleTabSwitch
    handleTabClose
    constructor(handleTabSwitch = empty, handleTabClose = empty, type, mode, position = []) {
        this.handleTabSwitch = handleTabSwitch
        this.handleTabClose = handleTabClose
        this.type = type
        this.mode = mode
        this.position = position
    }
}
class StoreCopy {
    timeStamp = new Date().getTime();
    fileList
    indexList
    favoriteList
    tagList
    indexTagRelationshipList
    indexFileRelationshipList
    indexCoverRelationshipList
    indexSubindexRelationshipList
}

function fileNameDetail(item) {
    const val = []
    val.push('<div style="display: flex;flex-wrap: wrap;flex-direction: column;">')
    for (const key of Object.keys(item)) {
        const name = $MyLibFileFieldName.get(key)
        if (name)
            val.push(`<p><strong>${name}</strong>:&nbsp;${item[key]}</p>`)
    }
    val.push('</div>')
    ElMessageBox.alert(
        val.join(''),
        {
            dangerouslyUseHTMLString: true,
            showConfirmButton: false,
            closeOnClickModal: true,
            showClose: false,
            draggable: true,
        }
    ).then(emptyFunction).catch(emptyFunction)
}
function indexNameDetail(item) {
    const val = []
    val.push('<div style="display: flex;flex-wrap: wrap;flex-direction: column;">')
    for (const key of Object.keys(item)) {
        const name = $MyLibIndexFieldName.get(key)
        if (name)
            val.push(`<p><strong>${name}</strong>:&nbsp;${item[key]}</p>`)
    }
    val.push('</div>')
    ElMessageBox.alert(
        val.join(''),
        // '详情',
        {
            dangerouslyUseHTMLString: true,
            showConfirmButton: false,
            closeOnClickModal: true,
            showClose: false,
            draggable: true,
        }
    ).then(emptyFunction).catch(emptyFunction)
}
function tagNameDetail(item) {
    console.log(item)
    const val = []
    val.push('<div style="display: flex;flex-wrap: wrap;flex-direction: column;">')
    for (const key of Object.keys(item)) {
        const name = $MyLibTagFieldName.get(key)
        if (name)
            val.push(`<p><strong>${name}</strong>:&nbsp;${item[key]}</p>`)
    }
    val.push('</div>')
    ElMessageBox.alert(
        val.join(''),
        // '详情',
        {
            dangerouslyUseHTMLString: true,
            showConfirmButton: false,
            closeOnClickModal: true,
            showClose: false,
            draggable: true,
        }
    ).then(emptyFunction).catch(emptyFunction)
}
const indexType = ['未知', '视频', '视频集', '图片', '文本', '文本集', '音频']

export { indexType, indexNameDetail, fileNameDetail, tagNameDetail, ListView, MetaView, NoneView,ResultEmptyView, TextContentView, PictureContentView, StoreCopy, Filter }