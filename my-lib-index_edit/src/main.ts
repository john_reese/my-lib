import { createApp } from 'vue'
import App from './App.vue'
import { myLibDbc } from './init.js'
import 'element-plus/dist/index.css'
import 'element-plus/theme-chalk/dark/css-vars.css'
import {
    create,
    NEllipsis
} from 'naive-ui'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

const naive = create({ components: [NEllipsis] })
const $APP = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    $APP.component(key, component)
}

myLibDbc
    .mount()
    .then(() =>
        $APP.use(naive).mount('#app'))
    .catch((error: Error) => {
        alert(error)
        console.log(error)
    })